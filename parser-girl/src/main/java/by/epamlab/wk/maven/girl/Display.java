package main.java.by.epamlab.wk.maven.girl;

import by.epamlab.wk.maven.HumanParser;
import by.epamlab.wk.maven.beans.Human;

public class Display {
    public static void main(String[] args){
        if(args.length != 0){
            sayHello(args[0]);
        }else{
            System.out.println("Missing parameter");
        }

    }

    public static void sayHello(String path) {
        HumanParser humanParser = new HumanParser(path);
        Human girl = humanParser.getHuman();
        System.out.println("Hello girl!" + girl.getName() + ";" + girl);
    }
}


