package main.java.by.epamlab.wk.maven.boy;

import by.epamlab.wk.maven.HumanParser;
import by.epamlab.wk.maven.beans.Human;

public class Display {
    public static void main(String[] args) {
        if (args.length != 0) {
            sayHello(args[0]);
        } else {
            System.out.println("Missing parameter");
        }
    }

    private static void sayHello(String path) {
        HumanParser humanParser = new HumanParser(path);
        Human boy = humanParser.getHuman();
        System.out.println("Hello boy!" + boy.getName() + ";" + boy);
    }
}