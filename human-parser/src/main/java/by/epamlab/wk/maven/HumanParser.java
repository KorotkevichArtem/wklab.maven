package by.epamlab.wk.maven;

import by.epamlab.wk.maven.beans.Human;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.TypeAdapter;

import java.io.FileReader;
import java.io.IOException;

public class HumanParser {
    private Human human;
    private static final TypeAdapter<Human> strictGsonObjectAdapter = new Gson().getAdapter(Human.class);

    public Human getHuman() {
        return human;
    }

    private Human getJson (String json) throws IOException {
            FileReader fr = new FileReader(json);
        return strictGsonObjectAdapter.fromJson(fr);
    }

    public HumanParser(String json){
        try {
            human = getJson(json);
        } catch (IOException e) {
            throw new JsonSyntaxException(e);
        }
    }
}
